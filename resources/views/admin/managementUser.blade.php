@extends('admin.index')

<!-- Isi Judul -->
@section('judul_halaman')
Management Users
@endsection

@section('konten')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Management Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Management Users</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- Isi Konten disisni -->
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <a class="btn btn-primary mb-3" href="/admin/add-user"><i class="fas fa-plus"></i> Tambah User</a>
                <div class="table">
                    <table class="table table-bordered table-hover">
                        <tr class="text-center">
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Pasword</th>
                            <th>Hak Akses</th>
                            <th>Aksi</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Admin</td>
                            <td>admin</td>
                            <td>123456</td>
                            <td>Admin</td>
                            <td class="text-center">
                                <a class="btn btn-primary" href="/admin/update-users"><i class="fas fa-pen"></i> Ubah</a>
                                <a class="btn btn-danger" href=""><i class="fas fa-trash"></i> Hapus</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jhon Doe</td>
                            <td>jhondoe</td>
                            <td>123456</td>
                            <td>Kontributor</td>
                            <td class="text-center">
                                <a class="btn btn-primary" href="/admin/update-users"><i class="fas fa-pen"></i> Ubah</a>
                                <a class="btn btn-danger" href=""><i class="fas fa-trash"></i> Hapus</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection