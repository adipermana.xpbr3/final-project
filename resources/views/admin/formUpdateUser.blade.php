@extends('admin.index')

<!-- Isi Judul -->
@section('judul_halaman')
Update User
@endsection

@section('konten')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Update User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin/dashboard">Home</a></li>
              <li class="breadcrumb-item"><a href="/admin/management-users">Management User</a></li>
              <li class="breadcrumb-item active">Update User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- Isi Konten disisni -->
    <div class="container-fuid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-8">
                        <form action="">
                            <div class="form-group">
                                <label for="">Nama User</label>
                                <input name="nama" type="text" class="form-control" value="Admin" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input name="username" type="text" class="form-control" value="Admin" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label for="">Password</label>
                                <input name="password" type="password" class="form-control" value="123456" required>
                            </div>
                            <div class="form-group">
                                <label for="">Hak Akses</label>
                                <select class="form-control" name="hak_akses" id="">
                                    <option value="">--Pilih Hak Akses--</option>
                                    <option value="1">Admin</option>
                                    <option value="2">Kontributor</option>
                                    <option value="3">User</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection