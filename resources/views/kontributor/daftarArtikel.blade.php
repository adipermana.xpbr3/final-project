@extends('kontributor.index')

<!-- Isi Judul -->
@section('judul_halaman')
Daftar Artikel
@endsection

@section('konten')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Daftar Artikel</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Daftar Artikel</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- Isi Konten disisni -->
    <div class="container-fluid">
      <div class="card">
        <div class="card-body">
          <a class="btn btn-primary mb-3" href="/kontributor/tambah-artikel"><i class="fas fa-plus"></i> Tambah Artikel</a>
          <div class="table">
            <table class="table table-bordered table-hover">
                <tr class="text-center">
                    <th>No</th>
                    <th>Judul</th>
                    <th>Isi Artikel</th>
                    <th>Penulis</th>
                    <th>Tanggal Dibuat</th>
                    <th>Aksi</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Laravel</td>
                    <td>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sequi laborum iure perferendis ab vitae reprehenderit ullam? Placeat quaerat similique repellat dignissimos tempore ipsam atque consequatur saepe qui minus hic nulla commodi facilis magnam, reprehenderit voluptas fugit libero iure ab asperiores, culpa expedita aperiam sed. Deserunt!</td>
                    <td>John Doe</td>
                    <td>2021-04-05</td>
                    <td>
                        <a class="btn btn-primary" href="/kontributor/update-artikel"><i class="fas fa-pen"></i> Ubah</a>
                        <a class="btn btn-danger" href=""><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>CRUD Laravel</td>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas omnis ad ut eos voluptate sed debitis tenetur incidunt minima quasi ipsam praesentium, rerum earum! Provident ab tempora ratione aliquid quaerat tempore ullam libero repellendus fugiat. Totam ab veniam magnam voluptate quidem inventore aperiam, ratione tempore qui ipsa placeat mollitia sed.</td>
                    <td>John Doe</td>
                    <td>2021-04-06</td>
                    <td>
                        <a class="btn btn-primary" href="/kontributor/update-artikel"><i class="fas fa-pen"></i> Ubah</a>
                        <a class="btn btn-danger" href=""><i class="fas fa-trash"></i> Hapus</a>
                    </td>
                </tr>

            </table>
          </div>
        </div>
      </div>
    </div>
    

</div>
@endsection